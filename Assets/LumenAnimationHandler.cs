﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LumenAnimationHandler : MonoBehaviour
{
    [SerializeField]
    private Animator lumensAnimator;
    private void Update()
    {
        string xAxis = "LeftJoystickX";
        string yAxis = "LeftJoystickY";
        // if lumen is moving animate him to run
        if(Input.GetAxis(xAxis) != 0|| Input.GetAxis(yAxis) != 0|| Input.GetKey(KeyCode.W)|| Input.GetKey(KeyCode.A)|| Input.GetKey(KeyCode.S)|| Input.GetKey(KeyCode.D))
        {
            print("Moving");
            lumensAnimator.SetBool("isMoving", true);
        }
        else
        {
            print("Not moving");
            lumensAnimator.SetBool("isMoving", false);
        }
        // if lumen is not moving animate him to idle
    }

}
