﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelMover : MonoBehaviour
{
    [Tooltip("The object whose position this model will be set to every frame.")]
    [SerializeField]
    private GameObject objectToTrack;

    [Tooltip("The model's pivot will be moved to the target's pivot each frame. The Y offset allows you to change where the model is moved to.")]
    [SerializeField]
    private float yOffset = 0;

    [Tooltip("Target direction tracker.")]
    [SerializeField]
    private DirectionOfMovementTracker targetMoveTracker;

    private void Start()
    {
        print("L key rigged");
    }

    private void LateUpdate()
    {
        //--------------
        // set this object's position to that of the object to track
        // factor in the y offset

        // get the target's coordinate
        Vector3 targetCoordinate = objectToTrack.transform.position;

        // modify it with the y offset
        targetCoordinate.y += yOffset;

        // set this object's position to that of the target coordinate + the Y offset
        transform.position = targetCoordinate;
        //--------------

        //***

        //--------------
        // rotate the object to match the player's direction of horizontal movement

        // get player movement vector
        Vector3 directionOfMovement = targetMoveTracker.GetDirectionOfMovement();

        // zero out y coord
        directionOfMovement.y = 0;

        // generate a quarternion facing it
        // apply newly made quarternion to this object's rotation
        Vector3 zeroVec = new Vector3(0, 0, 0);
        if(directionOfMovement != zeroVec)
            transform.rotation = Quaternion.LookRotation(directionOfMovement, Vector3.up);

        //--------------
    }


}
