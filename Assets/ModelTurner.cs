﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ModelTurner : MonoBehaviour
{


    [Tooltip ("Drag in the player model here.")]
    [SerializeField]
    private GameObject modelToTurn;
    [SerializeField]
    private DirectionOfMovementTracker movementTracker;

    // This script is designed to make the player's model rotate to face their direction of movement.
    private void Update()
    {
        // get direction of movement
        Vector3 playerMovementDirection = movementTracker.GetDirectionOfMovement();

        // set model angle to face it
        modelToTurn.transform.rotation = Quaternion.FromToRotation(modelToTurn.transform.forward, playerMovementDirection);
    }
}
