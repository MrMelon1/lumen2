﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseMenu : MonoBehaviour
{
    public static bool IsGamePaused = false;

    [Tooltip("Drag in the canvas element containing all\nstuff related to the pause menu.")]
    [SerializeField]
    private GameObject UI;
    [Tooltip("Drag in the script attached to the\nplayer called PauseForUI.")]
    [SerializeField]
    private PauseForUI controllerDisabler;
    [SerializeField]
    private string pauseButton = "Start";

    private bool isAllowedToRun = false;

    private void Start()
    {
        if (UI == null || controllerDisabler == null || pauseButton == null)
        {
            print("THE PAUSE MENU SCRIPT VARIABLES NEED TO BE CONFIGURED");
        }
        else
            isAllowedToRun = true;
    }

    private void Update()
    {
        if (isAllowedToRun)
        {
            // if the player presses ESC or START
            if (Input.GetButtonDown(pauseButton))
            {
                // if the game is not paused
                if (!IsGamePaused)
                {
                    Pause();
                }
                else
                {
                    // if the game is paused
                    Resume();
                }
            }
        }
    }

    private void Pause()
    {
        IsGamePaused = true;

        controllerDisabler.setControlsEnabled(false);


        UI.SetActive(true);
        Time.timeScale = 0f;
    }

    public void Resume()
    {
        IsGamePaused = false;

        // we need to replace this portion with a call to our PauseForUI script
        controllerDisabler.setControlsEnabled(true);


        UI.SetActive(false);
        Time.timeScale = 1f;
    }

    public void Quit()
    {
        Application.Quit();
    }
}
