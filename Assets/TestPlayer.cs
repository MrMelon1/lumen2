﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TestPlayer : MonoBehaviour
{
    public CharacterController controller;
    public float speed = 6.0f;
    public float rotateSpeed = -6f;
    public float jumpSpeed = 8f;
    public float gravity = 20f;

    private Vector3 moveDirection = Vector3.zero;
    private void Start()
    {
        print("F KEY");
    }
    private void Update()
    {
        moveDirection = new Vector3(Input.GetAxis("Horizontal"), 0, Input.GetAxis("Vertical"));
        moveDirection = transform.TransformDirection(moveDirection);
        moveDirection *= speed;

        moveDirection.y -= gravity * Time.deltaTime;
        if(Input.GetKeyDown(KeyCode.F))
            print(moveDirection);
        controller.Move(moveDirection * Time.deltaTime);
    }

}
