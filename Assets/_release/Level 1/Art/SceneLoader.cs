﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    public string sceneToBeLoaded;
    public Vector3 positionA;
    public Vector3 positionB;

    private void Start()
    {
        transform.position = positionA;
    }

    private void OnTriggerEnter(Collider other)
    {
        // Load the next scene.
        if (transform.position == positionA)
        {
            SceneManager.LoadSceneAsync(sceneToBeLoaded, LoadSceneMode.Additive);
            transform.position = positionB;
        }
        else
        {
            List<LevelRoot> allLevels = new List<LevelRoot>(FindObjectsOfType<LevelRoot>());
            Destroy(allLevels.Find(x => x.childSceneName == sceneToBeLoaded).gameObject);
            transform.position = positionA;
        }
        // Move this to point b
        // if it is already at point b move it to point a
    }
}
