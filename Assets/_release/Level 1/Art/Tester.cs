﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Tester : MonoBehaviour
{
    public GameObject objectA;
    public GameObject objectB;

    // Test 003
    // Need to test linecast to see if beam can be interupted
    // or if it actually reports interuptions.

    private void Update()
    {
        RaycastHit hit;
        Debug.DrawLine(objectA.transform.position, objectB.transform.position);
        if (Physics.Linecast(objectA.transform.position, objectB.transform.position, out hit))
            print("Hit!");
    }
}
