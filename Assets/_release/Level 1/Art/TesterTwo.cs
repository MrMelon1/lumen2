﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TesterTwo : MonoBehaviour
{
    public GameObject a;
    public GameObject b;

    private void Update()
    {
        Debug.DrawLine(a.transform.position, b.transform.position);
    }
}
