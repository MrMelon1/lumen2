﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class TimeBox : MonoBehaviour
{
    private void Start()
    {
        print("The Z key is rigged to slow down time!");
    }
    private void Update()
    {
        if (Input.GetKeyDown(KeyCode.Z))
        {
            if (Time.timeScale == 1.0f)
                Time.timeScale = .05f;
            else
                Time.timeScale = 1.0f;
        }
    }
}
