﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AbilitySFXManager : MonoBehaviour
{
  
	AudioSource SFXEchoDeploy;
	AudioSource SFXEchoActivate;

    void Start() //Instantiate all sfx objects
    {
		SFXEchoDeploy = GameObject.Find("EchoDeploy").GetComponent<AudioSource>();
		SFXEchoActivate = GameObject.Find("EchoActivate").GetComponent<AudioSource>();
    }

	public void EchoDeploy() //Play Deploy Sound
	{
		SFXEchoDeploy.Play();
	}

	public void EchoActivate() //Play Activate Sound
	{
		SFXEchoActivate.Play();
	}
 
 
}
