﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Button : MonoBehaviour
{
	AudioSource SFXDown;
	AudioSource SFXUp;
    private bool isOn = false;
    private float moveDistance = .24f;
    private List<GameObject> collisionObjectList = new List<GameObject>();

    public bool getIsOn() { return isOn; }

    private void Start()
    {
		SFXDown = GameObject.Find("ButtonDown").GetComponent<AudioSource>();
		SFXUp = GameObject.Find("ButtonUp").GetComponent<AudioSource>();
    }

    private void Update()
    {
        /*if (Input.GetKeyDown(KeyCode.Q) && collisionObjectList.Count != 0)
        {
            print(collisionObjectList.Count);
            print(collisionObjectList[0].gameObject.name);
        }*/
        // If an element in the list is null
        // Remove it
        if(collisionObjectList.Count > 0)
            for (int i = 0; i < collisionObjectList.Count; i++)
                if (collisionObjectList[i].Equals(null))
                {
                    collisionObjectList.RemoveAt(i);
                    checkCountToToggle();
                }
    }

    private void OnTriggerEnter(Collider other)
    {

        collisionObjectList.Add(other.gameObject);
		SFXDown.Play();
        

        // move the button down
        if (collisionObjectList.Count == 1)
        {
            transform.Translate(Vector3.down * moveDistance);
            isOn = true;
        }
        
    }

    private void OnTriggerExit(Collider other)
    {
        collisionObjectList.Remove(other.gameObject);
		SFXUp.Play();
        // move the button up
        if (collisionObjectList.Count <= 0) // mark
        {
            transform.Translate(Vector3.up * moveDistance);
            isOn = false;
        }
        
    }

    private void checkCountToToggle()
    {
        if (collisionObjectList.Count <= 0) // mark
        {
            transform.Translate(Vector3.up * moveDistance);
            isOn = false;
        }
    }

}
