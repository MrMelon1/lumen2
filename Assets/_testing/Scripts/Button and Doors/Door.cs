﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
	public AudioSource SFXOpen;
	public AudioSource SFXClose;
    public List<Button> buttons;
    public float openCloseSpeed = 1;

    [SerializeField]
    protected Vector3 newPosition;
    protected float moveDistance = 20.0f;
    protected bool isOpen = false;
	void Start()
	{
        //	SFXOpen = GameObject.Find("DoorOpen").GetComponent<AudioSource>();
        //	SFXClose = GameObject.Find("DoorClose").GetComponent<AudioSource>();
        newPosition = transform.position;
	}

     protected bool checkIfButtonIsOn(int buttonListNumber)
    {
        if (buttonListNumber >= buttons.Count)
            return false;
        bool x = buttons[buttonListNumber].getIsOn();
        return x;
    }

    protected bool checkIfAllButtonsAreOn()
    {
        int buttonsOn = 0;
        for (int i = 0;i<buttons.Count;i++)
        {
            if (buttons[i].getIsOn())
                buttonsOn += 1;
        }
        if (buttonsOn == buttons.Count)
            return true;
        else
            return false;
    }

    protected bool checkIfAnyButtonsAreOn()
    {
        for (int i = 0; i < buttons.Count; i++)
        {
            if (checkIfButtonIsOn(i))
                return true;
        }
        return false;
    }

    protected void toggleNewPosition()
    {
        Vector3 moveFactor = Vector3.up * moveDistance;
        // if its open
        // set new position to the open position

        if (isOpen)
        {
            newPosition = newPosition - moveFactor;
            SFXOpen.Play();
        }

        // if its closed
        // set new position to the closed position
        if (!isOpen)
        {
            newPosition = newPosition + moveFactor;
            SFXClose.Play();
        }
    }

    protected void open()
    {
        transform.Translate(Vector3.down * moveDistance);
		SFXOpen.Play();
    }

    protected void close()
    {
        transform.Translate(Vector3.up * moveDistance);
		SFXClose.Play();
    }

    protected void moveToNewPosition()
    {
        if(transform.position != newPosition)
            transform.position = Vector3.Lerp(transform.position, newPosition, Time.deltaTime * openCloseSpeed);
    }
}
