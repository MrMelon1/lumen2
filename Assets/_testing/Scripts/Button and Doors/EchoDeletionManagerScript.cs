﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class EchoDeletionManagerScript : MonoBehaviour
{
    //Ok, here's the last test.

    // I want to see how removing an item from a list affacts all of the indexes.

    // We know that removing an item is not the same as setting it to null, otherwise null statements would've been printed by now.

    // So here's what we're going to do:

    // Create a list of items.
    List<string> list = new List<string>(3);
    // Print all the items, and their index.
    private void Start()
    {
        list.Add("apple");
        list.Add("bannana");
        list.Add("orange");
        for (int i = 0; i < list.Count; i++)
            print("Index: " + i + "\n" + "Item: " + list[i]);
        print("Removing the bannana.");
        list.Remove("bannana");
        for (int i = 0; i < list.Count; i++)
            print("Index: " + i + "\n" + "Item: " + list[i]);
        print("Setting orange to null");
        list[1] = null;
        for (int i = 0; i < list.Count; i++)
            print("Index: " + i + "\n" + "Item: " + list[i]);

    }

    /*
    OK! And so that concludes our research expedition!

    What we learned:
    Removing an item from a list is not the same thing as setting it to null.
    When we remove the item from the list, all the indexes get shifted as units slide down to fill the gap.
    If we set an item to null, the count will remain the same.

    Conclusion:
    For the project, we need to create a list have and have units 
    dynamically populate and vacate it as the button is collided with.
    We also need to set up, in the Update() loop, a test to check to see 
    if any of the units are set to null.
    If they are? Remove them.

    If you tweak your code to use the count of the list instead of an 
    int value
    and implement the other changes as well
    then the button should not only work, but it should also be 
    compatible with other objects too!!!!

    SO lets do it!!!!!

    // Action List
    // Change button to use a list instead of a collision count.
    // Set it up to remove null elements from the list constantly.
    */

}
