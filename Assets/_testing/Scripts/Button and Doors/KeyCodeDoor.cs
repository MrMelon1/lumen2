﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KeyCodeDoor : Door
{

    // input needed to open door
    public int inputNeeded = 3;
    // current correct input value
    private int currentCorrectButton = 0;
    private int lastCorrectButton = 0;
    private bool alreadyToggled = false;




    // Update is called once per frame
    void Update()
    {

        // a button is pressed
        if (checkIfAnyButtonsAreOn() && !isOpen)
        {
            // if it is the correct one we update the value of the current correct input value and last correct input value
            if (checkIfButtonIsOn(currentCorrectButton))
            {
                lastCorrectButton = currentCorrectButton;
                currentCorrectButton += 1;
                
            }
            // else if it is the last correct button we do nothing
            else if (checkIfButtonIsOn(lastCorrectButton))
            {

            }
            else
            {
                currentCorrectButton = 0;
                
            }
            // else we reset the value of the current correct input value back to zero and the last correct button to null
        }


        // if the current correct input value equals the number of inputs needed to open the door
        if(currentCorrectButton == inputNeeded && !alreadyToggled)
        {
            alreadyToggled = true;
            isOpen = true;
            toggleNewPosition();
        }
        // open it if it isn't already!
        moveToNewPosition();
    }
}
