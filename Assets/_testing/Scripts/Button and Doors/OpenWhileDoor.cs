﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OpenWhileDoor : Door
{


    // Update is called once per frame
    void Update()
    {
        if (!isOpen && checkIfAllButtonsAreOn())
        {
            isOpen = true;
            toggleNewPosition();
        }
        else if (isOpen && !checkIfAllButtonsAreOn())
        {
            isOpen = false;
            toggleNewPosition();
        }
        moveToNewPosition();
    }
}
