﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class StayOpenDoor : Door
{
    


    
    void Update()
    {
        // if all the buttons are pressed
        // open the door

        if (checkIfAllButtonsAreOn() && !isOpen)
        {
            isOpen = true;
            toggleNewPosition();
        }
        moveToNewPosition();
    }
}
