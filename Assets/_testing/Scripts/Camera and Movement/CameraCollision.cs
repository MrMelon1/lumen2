﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraCollision : MonoBehaviour
{
    public float minDistance = 1.0f;
    public float maxDistance = 4.0f;
    public float smooth = 10.0f;
    Vector3 dollyDir;
    public Vector3 dollyDirAdjusted;
    public float distance;

    private void Awake()
    {
        dollyDir = transform.localPosition.normalized;
        distance = transform.localPosition.magnitude;
    }

    // Update is called once per frame
    void Update()
    {
        float ninetyPercant = .9f;
        Vector3 desiredCameraPos = transform.parent.TransformPoint(dollyDir * maxDistance);
        RaycastHit hit;
        if (Physics.Linecast (transform.parent.position, desiredCameraPos, out hit))
        {
            if(hit.collider.tag == "ignoreCameraCollision")
            {
                // Do nothing if the camera rubs up against something we mark as invisible.
                // TODO assess this code to see if it causes problems with camera collisions down the line.
            }
            else
                distance = Mathf.Clamp((hit.distance * ninetyPercant), minDistance, maxDistance);
        }
        else
        {
            distance = maxDistance;
        }


        transform.localPosition = Vector3.Lerp(transform.localPosition, dollyDir * distance, Time.deltaTime * smooth);
    }
}
