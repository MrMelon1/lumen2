﻿using UnityEngine;
using System.Collections;

public class CameraControllerScript : MonoBehaviour
{
    Vector2 mouseLook;
    Vector2 smoothV;
    public float sensitivity = 5.0f;
    public float smoothing = 2.0f;

    GameObject character;
    Camera mainCam;

    void Start()
    {
        character = this.transform.parent.gameObject;
        mainCam = GameObject.Find("Main Camera").GetComponent<Camera>();
    }

    void Update()
    {
        string xAxis = "RightJoystickX";
        string yAxis = "RightJoystickY";
        int negativeIfControllerActive = -1;

        // if mouse input is detected
        // change the axes
        if(Input.GetAxisRaw("Mouse X") != 0 || Input.GetAxisRaw("Mouse Y") != 0)
        {
            xAxis = "Mouse X";
            yAxis = "Mouse Y";
            negativeIfControllerActive = 1;
        }

        var md = new Vector2(Input.GetAxisRaw(xAxis), negativeIfControllerActive * Input.GetAxisRaw(yAxis));

        md = Vector2.Scale(md, new Vector2(sensitivity * smoothing, sensitivity * smoothing));
        smoothV.x = Mathf.Lerp(smoothV.x, md.x, 1f / smoothing);
        smoothV.y = Mathf.Lerp(smoothV.y, md.y, 1f / smoothing);
        mouseLook += smoothV;
        mouseLook.y = Mathf.Clamp(mouseLook.y, -70, 80);

        transform.localRotation = Quaternion.AngleAxis(-mouseLook.y, Vector3.right);
        character.transform.localRotation = Quaternion.AngleAxis(mouseLook.x, character.transform.up);

        CompensateForWalls(character.transform.position, mainCam.transform.position);
    }

    private void CompensateForWalls(Vector3 subject, Vector3 cam)
    {
        RaycastHit wallHit = new RaycastHit();
        if(Physics.Linecast(subject, cam, out wallHit))
        {
            cam = new Vector3(wallHit.point.x, cam.y, wallHit.point.z);
        }
    }
}