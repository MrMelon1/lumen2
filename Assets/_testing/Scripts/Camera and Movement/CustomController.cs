﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CustomController : MonoBehaviour
{
    //move variables
    public float speed = 10.0f;
    public LayerMask groundLayers;

    //jump variables
    public float jumpForce = 7;
    private Rigidbody rb;

    //echo mechanic
    public GameObject preEcho;
    public GameObject echo;
    public GameObject movingEcho;
    public bool isTouchingEcho;
    public Collider thisCollider;

    // Start is called before the first frame update
    void Start()
    {
        //for movement
        Cursor.lockState = CursorLockMode.Locked;

        //for jumping
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {

        //movement
        float translation = Input.GetAxis("Vertical") * speed;
        float straffe = Input.GetAxis("Horizontal") * speed;
        translation *= Time.deltaTime;
        straffe *= Time.deltaTime;

        //echo
        Collider eCollider = echo.GetComponent<Collider>(); //This line only works with game objects, not plain old objects.

        transform.Translate(straffe, 0, translation);

        if (Input.GetKeyDown("escape"))
        {
            Cursor.lockState = CursorLockMode.None;
            print(gameObject.name);
        }

        //jump
        if (IsGrounded() && Input.GetKeyDown(KeyCode.Space))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }

        //echo
        if (Input.GetButtonDown("Detonate")) //when e key is pressed
        {

            if (preEcho == null)
            {
                Instantiate(preEcho, transform.position, transform.rotation);
            }
                
            
            
            /*LayerMask.NameToLayer("Player");
            if (IsPressingWASD())
                Instantiate(movingEcho, transform.position, transform.rotation);
            else
                Instantiate(echo, transform.position, transform.rotation);
                */
        }
        //If the player is not longer intersecting with the object, swap its layer.
        /*if (!thisCollider.bounds.Intersects(eCollider.bounds))
        {
            Debug.Log("Bounds intersecting");
        }*/


    }

    private bool IsGrounded()
    {
        RaycastHit hit;
        float distance = 1f;
        Vector3 dir = new Vector3(0, -1);

        if (Physics.Raycast(transform.position, dir, out hit, distance))
        {
            return true;
        }
        else
        {
            return false;
        }
    }

    private bool IsPressingWASD() //For seeing if the player is moving using WASD
    {
        if (Input.GetKey("w"))
            return true;
        else if (Input.GetKey("a"))
            return true;
        else if (Input.GetKey("s"))
            return true;
        else if (Input.GetKey("d"))
            return true;
        else
            return false;
    }
}
