﻿using UnityEngine;
// part of the code comes from https://www.youtube.com/watch?v=7KiK0Aqtmzc
public class JumpController : MonoBehaviour
{
    
    public float jumpForce = 7;
    public LayerMask ground;
    public float fallMultiplier = 2.5f;
    public float lowJumpMultiplier = 2f;
    public string jumpControlButton;

    private Rigidbody rb;
    private CapsuleCollider col;

    // Start is called before the first frame update
    void Start()
    {
        col = GetComponent<CapsuleCollider>();
        rb = GetComponent<Rigidbody>();
    }

    // Update is called once per frame
    void Update()
    {
        // if a keyboard input is detected
        // modify the condition to check for 
        if ((IsGrounded() && Input.GetButtonDown(jumpControlButton))||(IsGrounded() && Input.GetKeyDown(KeyCode.Space)))
        {
            rb.AddForce(Vector3.up * jumpForce, ForceMode.Impulse);
        }
        if (rb.velocity.y < 0)
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (fallMultiplier - 1) * Time.deltaTime;
        } else if((rb.velocity.y>0 && !Input.GetButtonDown(jumpControlButton)) || (rb.velocity.y > 0 && !Input.GetKeyDown(KeyCode.Space)))
        {
            rb.velocity += Vector3.up * Physics.gravity.y * (lowJumpMultiplier - 1) * Time.deltaTime;
        }
    }

    private bool IsGrounded()
    {
        float maxDistance = 1.2f;
        float debugRayDuration = 0f;
        Debug.DrawRay(new Vector3(col.bounds.center.x, col.bounds.center.y, col.bounds.center.z), Vector3.down * maxDistance, Color.green, debugRayDuration, true);
        //if (Physics.Raycast(new Vector3(col.bounds.center.x, col.bounds.min.y, col.bounds.center.z), Vector3.down, maxDistance, ground))
        if (Physics.Raycast(new Vector3(col.bounds.center.x, col.bounds.center.y, col.bounds.center.z), Vector3.down, maxDistance, ground))
            return true;
        else return false;
    }

}
