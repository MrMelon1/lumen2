﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MovementController : MonoBehaviour
{
    private bool isMoving = false;

    //public CharacterController controller;
    public float speed = 10.0f;
    public LayerMask groundLayers;

    // Start is called before the first frame update
    void Start()
    {
        Cursor.lockState = CursorLockMode.Locked;
    }

    // Update is called once per frame
    void Update()
    {
        string xAxis = "LeftJoystickX";
        string yAxis = "LeftJoystickY";
        int negativeIfUsingController = -1;

        // if using keyboard
        // change axes
        if (IsPressingWASD())
        {
            xAxis = "Horizontal";
            yAxis = "Vertical";
            negativeIfUsingController = 1;
        }

            if (Input.GetAxis(yAxis) != 0 || Input.GetAxis(xAxis) != 0)
            {
                float translation = negativeIfUsingController * Input.GetAxis(yAxis) * speed;
                float straffe = Input.GetAxis(xAxis) * speed;
                translation *= Time.deltaTime;
                straffe *= Time.deltaTime;

                transform.Translate(straffe, 0, translation);
                //Vector3 moveVector = new Vector3(straffe, 0, translation);
                //controller.Move(moveVector * Time.deltaTime);
            }
            else
            {
                Rigidbody rb = GetComponent<Rigidbody>();
                rb.velocity = new Vector3(0, rb.velocity.y, 0);
            }
    }

    private bool IsUsingAnalogSticks()
    {
        if (Input.GetAxis("LeftJoystickX") != 0)
            return true;
        else if (Input.GetAxis("LeftJoystickY") != 0)
            return true;
        else
            return false;
    }

    private bool IsPressingWASD() //For seeing if the player is moving using WASD
    {
        if (Input.GetKey("w"))
            return true;
        else if (Input.GetKey("a"))
            return true;
        else if (Input.GetKey("s"))
            return true;
        else if (Input.GetKey("d"))
            return true;
        else
            return false;
    }
}
