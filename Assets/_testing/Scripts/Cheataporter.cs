﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Cheataporter : MonoBehaviour
{

    public SubCheataporter a;
    public SubCheataporter b;

    private void Update()
    {
        if (a.getIsTouched())
        {
            b.setHasPermission(false);
            GameObject.Find("Player").transform.position = b.transform.position;
            a.setIsTouched(false);
        }
        if (b.getIsTouched())
        {
            a.setHasPermission(false);
            GameObject.Find("Player").transform.position = a.transform.position;
            b.setIsTouched(false);
        }
    }

}
