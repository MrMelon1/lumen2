﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CheckpointSpawnee : MonoBehaviour
{
    public SafeZone lastTouchedSafeZone;
    public void setLastTouchedSafeZone(SafeZone sf) { lastTouchedSafeZone = sf; }

    public void sendToSpawn()
    {
        GetComponent<HealthBar>().setHealthToMax();
        transform.position = lastTouchedSafeZone.transform.position;
    }
}
