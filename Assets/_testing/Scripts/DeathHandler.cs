﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class DeathHandler : MonoBehaviour
{
    public void triggerDeathSequence()
    {
        // Later on this method will do a whole fancy shabang where it triggers a moody death animation. For now, its going to send the player back to the main menu.
        SceneManager.LoadScene("TitleScreen");
    }
}
