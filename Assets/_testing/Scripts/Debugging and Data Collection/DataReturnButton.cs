﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DataReturnButton : MonoBehaviour
{
    Rigidbody rb;

    private void Start()
    {
        rb = GetComponent<Rigidbody>();
    }


    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.I))
        {
            print("Rotation: " + transform.rotation);
        }
        if (Input.GetKeyDown(KeyCode.K))
        {
            print("Velocity: " + rb.velocity);
        }
        if (Input.GetKeyDown(KeyCode.M))
        {
            print("transform.forward: " + transform.forward);
        }
        if (Input.GetKeyDown(KeyCode.Comma))
        {
            GameObject player = GameObject.Find("Player");
            DirectionOfMovementTracker tracker = player.GetComponent<DirectionOfMovementTracker>();
            print("direction of movement: " + tracker.GetDirectionOfMovement());
        }
    }
}
