﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DirectionOfMovementTracker : MonoBehaviour
{
    private Vector3 storedPosition;

    public float updateRate = 0.1F;

    // Start is called before the first frame update
    void Start()
    {
        InvokeRepeating("savePosition", updateRate, updateRate);
    }

    private void savePosition()
    {
        storedPosition = transform.position;
    }

    public Vector3 GetDirectionOfMovement() { return transform.position - storedPosition; }
    public Quaternion GetAngleOfMovement()
    {
        storedPosition.y = transform.position.y;
        Quaternion rotation = Quaternion.LookRotation((transform.position - storedPosition), Vector3.up);
        return rotation;
    }
}
