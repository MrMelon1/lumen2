﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class DevUIController : MonoBehaviour
{
    [SerializeField]
    private GameObject mainContainer;
    [SerializeField]
    private InputField input;
    [SerializeField]
    private Text output;

    public UIEventExecutor eventExecutor;

    private bool isGoodToRun = false;

    private void Start()
    {
        if (!mainContainer && !input && !output)
            print("ERROR: NOT ALL OF THE VARIABLES HAVE BEEN HOOKED UP YET");
        else
            isGoodToRun = true;
    }

    private void Update()
    {
        if (isGoodToRun)
        {
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                if (!mainContainer.active)
                {
                    mainContainer.SetActive(true);
                    input.Select();
                }
                else if (mainContainer.active)
                {
                    mainContainer.SetActive(false);
                }
            }

            if (Input.GetKeyDown(KeyCode.KeypadEnter))
            {
                //output.text = input.text;
                //input.text = "";
                List<string> proccessedInput = new List<string>(input.text.Split(' '));
                PerformOperation(proccessedInput);
            }
        }
    }

    private void PerformOperation(List<string> line)
    {
        output.text = eventExecutor.performActionAndReturnOutput(line);
        input.text = "";
        input.Select();
    }
}
