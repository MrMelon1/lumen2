﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.AI;

public class Imp : Enemy
{
    [SerializeField]
    // we need a list of destinations for it to bounce to and from
    // default size is two
    // the way it will ultimately work is that the enemy will work up and down the list of coordinates in order to patrol back and forth
    public List<GameObject> patrolPointMarkers = new List<GameObject>(2); // do not use solid objects as markers!
    public int stamina = 1; // determines how many moves enemy can make before needing to rest
    public int maxStamina = 1;
    public float sightRadius = 5.0f;
    public float huntingSpeed = 9.5f;
    public float abandonChaseTime = 2.0f;

    private NavMeshAgent enemyAgent;
    [SerializeField]
    private int patrolPointCount = 0;
    private int addToCount = 1;
    [SerializeField]
    private string status;
    private GameObject targetToTrack;
    private bool isInEnemyZone = true;
    private List<Vector3> patrolPoints = new List<Vector3>();

    // send the enemy to its destination
    private void Start()
    {
        for (int i = 0; i < patrolPointMarkers.Count; i++)
            patrolPoints.Add(patrolPointMarkers[i].transform.position);
        targetToTrack = GameObject.Find("Player");
        if (!targetToTrack)
            print("ERROR: PLAYER COULD NOT BE FOUND");
        status = "patrolling";
        enemyAgent = GetComponent<NavMeshAgent>();
    }

    // if the enemy reaches it
    // make it head back to the next one
    // repeat until it reaches end
    // pause, then start backtracking

    private void Update()
    {
        // if the player is within range of the enemy
        // stop it from moving
        if(targetToTrack)
            if (Vector3.Distance(transform.position, targetToTrack.transform.position) <= sightRadius && status != "idle")
            {
                // if the player is in range of the enemy have the enemy turn and move towards the player constantly
                // stop patrolling
                status = "aggravated";
            }
        // if the player is in range of the enemy have the enemy turn and move towards the player constantly
        if (!enemyAgent.hasPath && status == "patrolling")
        {
            moveToNextPoint();
            stamina -= 1;
        }
        if(status == "aggravated")
        {
            if (enemyAgent.enabled)
            {
                enemyAgent.destination = transform.position;
                enemyAgent.enabled = false;
            }
            // set angle towards the players direction
            // is normalized to prevent enemy from tilting up or down
            Vector3 targoVector = targetToTrack.transform.position;
            targoVector.y = transform.position.y;
            transform.LookAt(targoVector);
            // move the enemy forward
            // IE towards the player
            transform.Translate(Vector3.forward * Time.deltaTime * huntingSpeed);
        }
    }

    private void moveToNextPoint()
    {
        enemyAgent.SetDestination(patrolPoints[patrolPointCount]);
        patrolPointCount += addToCount;
        if (patrolPointCount == patrolPoints.Count - 1)
            addToCount = -1;
        if (patrolPointCount == 0)
            addToCount = 1;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.tag == "enemyPatrolZone")
            StartCoroutine(endChase());
    }

    IEnumerator endChase()
    {
        // make the enemy wait
        status = "idle";
        yield return new WaitForSeconds(abandonChaseTime);
        // reset their position
        patrolPointCount = 0;
        addToCount = 1;
        transform.position = patrolPoints[patrolPointCount];
        enemyAgent.enabled = true;
        status = "patrolling";
    }

    private void OnCollisionEnter(Collision collision)
    {
        if(collision.gameObject.name == "Player")
        {
            collision.gameObject.GetComponent<HealthBar>().dealDamage(1);
        }
    }
}
