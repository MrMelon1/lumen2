﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenLightObject : LightObject
{
    private GameObject reference;

    private void Awake()
    {
        reference = gameObject;
    }

    new public GameObject getReference()
    {
        return reference;
    }

    private void Update()
    {
        if (reference == null)
        {
            print("destroyed");
            Destroy(gameObject);
        }
    }

    private void OnTriggerEnter(Collider collision)
    {
        if (collision.gameObject.GetComponent<GreenLightGlue>())
        {
            print(collision.gameObject.name);
            reference = collision.gameObject;
            // disable mesh renderer and further collisions
            makeInvisibleAndPhaseThrough();
        }
    }

    private void makeInvisibleAndPhaseThrough()
    {
        gameObject.GetComponent<MeshRenderer>().enabled = false;
        gameObject.GetComponent<SphereCollider>().enabled = false;
    }
}
