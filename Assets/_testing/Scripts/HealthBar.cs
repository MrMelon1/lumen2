﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class HealthBar : MonoBehaviour
{
    public int maxHealth = 1;
    public int health = 1;
    public Vector3 checkpointCoordinate;

    private SafeZone lastTouchedSafeZone;

    public void dealDamage(int incomingDamage) { health -= incomingDamage; }
    public void setHealthToMax() { health = maxHealth; }

    private void Update()
    {
        if (health <= 0)
        {
            print(lastTouchedSafeZone);
            if (lastTouchedSafeZone)
            {
                if (lastTouchedSafeZone.getUses() > 0)
                {
                    // if the checkpoint has any uses left
                    transform.position = checkpointCoordinate;
                    health = maxHealth;
                    lastTouchedSafeZone.setUses(lastTouchedSafeZone.getUses() - 1);
                }
                else
                    triggerDeathSequence();
                // else run the death handler function
            }
            else
            {
                triggerDeathSequence();
            }
        }
    }
    private void OnTriggerEnter(Collider other)
    {
        if (other.GetComponent<SafeZone>())
        {
            lastTouchedSafeZone = other.GetComponent<SafeZone>();
            if (lastTouchedSafeZone)
                checkpointCoordinate = transform.position;
        }
    }

    private void triggerDeathSequence()
    {
        // Later on this method will do a whole fancy shabang where it triggers a moody death animation. For now, its going to send the player back to the main menu.
        SceneManager.LoadScene("TitleScreen");
    }
}
