﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class KillZone : MonoBehaviour
{
    private void OnTriggerEnter(Collider other)
    {
        int overkill = 999;
        if (other.gameObject.GetComponent<MovementController>())
            other.gameObject.GetComponent<HealthBar>().dealDamage(overkill);
    }
}
