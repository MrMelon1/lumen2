﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

// YO
// MAKE SURE ANY OBJECT WITH THIS SCRIPT
// IS CENTERED AT 0, 0, 0!!!!

// Entire levels are going to be set as children, so zeroing this out will potentially
// save someone a headache in the far future...
// potentially.

public class LevelRoot : MonoBehaviour
{
    public string childSceneName; // make this the same as the name of the scene it starts off in!!!
    

    // This script is just here to make the removal of added levels easier.
    // According to my research, there is no unload scene function with unity.
    // The only way to get rid of a scene is by storing a reference to all
        // the objects in it and delteting them all.
    // Thats what this script is for, tagging objects that are intended
        // to hold a reference to all the assets in each level.
}
