﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class FadeController : MonoBehaviour
{
    private Image image;
    private bool hasPermissionToRun = false;
    private bool isMeantToBeVisibile = false;
    private float fadeSpeed = .25f;
    
    // Start is called before the first frame update
    void Start()
    {
        image = GetComponent<Image>();
        if (image != null)
            hasPermissionToRun = true;
        else
            print("ERROR: THIS OBJECT DOES NOT HAVE AN IMAGE COMPONENT TO WORK WITH");
    }

    private void Update()
    {
        float fullAlpha = 1f;
        float emptyAlpha = 0f;
        if (hasPermissionToRun)
        {
            // shift the alpha towards the correct value
            // the correct value changes depending on whether or not this object is meant to be visible
            if (isMeantToBeVisibile)
                ShiftAlphaTo(fullAlpha);
            else
                ShiftAlphaTo(emptyAlpha);
        }
    }

    public void FadeIn(float inputTime)
    {
        if (!hasPermissionToRun)
        {
            print("This object does not have permission to run yet!");
            return;
        }

        // shift the correct value to visisible
        isMeantToBeVisibile = true;

        
    }
    public void FadeOut(float inputTime)
    {
        if (!hasPermissionToRun)
        {
            print("This object does not have permission to run yet!");
            return;
        }
        // shift the correct value to be invisible
        isMeantToBeVisibile = false;
    }

    private void ShiftAlphaTo(float newAlpha)
    {
        Color tempColor = image.color;
        if (tempColor.a != newAlpha)
        {
            tempColor.a += Mathf.Lerp(tempColor.a, newAlpha, Time.deltaTime*fadeSpeed);
            image.color = tempColor;
        }
    }
}
