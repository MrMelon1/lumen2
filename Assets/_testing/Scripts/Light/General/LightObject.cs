﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightObject : MonoBehaviour
{
    protected float duration = 10f;

    public void setDuration(float f) { duration = f; }

    public virtual void Detonate(Quaternion playerRotation, float speed)
    {
    }

    public void setSpeed(float speed)
    {

    }

    public GameObject getReference()
    {
        print("The parent was called");
        return null;
    }
}
