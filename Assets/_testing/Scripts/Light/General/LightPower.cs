﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LightPower : MonoBehaviour
{
    // commonalities between all powers
    // on/off toggle
    public bool active = true;
    public float maxDistanceFromPlayer = 20f;
    public float cooldown = 1f;
    public float breakAwayCooldown = 1f;
    [Tooltip("Options:\nX\nY\nB\nA\nLeftTrigger\nRightTrigger\nLeftBumper\nRightBumper")]
    public string powerControlButton;
    [Tooltip("Button for activating power without controller.")]
    public string controlBtnKeyboardAlternate;

    protected KeyCode controlBtnAltKeycode;
    protected LightObject lightObjectReference = null;
    protected bool isPowerInUse = false;

    protected void Start()
    {
        controlBtnAltKeycode = (KeyCode)System.Enum.Parse(typeof(KeyCode), controlBtnKeyboardAlternate);
    }

    protected IEnumerator Cooldown()
    {
        active = false;
        yield return new WaitForSeconds(cooldown);
        active = true;
    }

    protected IEnumerator Cooldown(float seconds)
    {
        print("Cooldown called.");
        active = false;
        yield return new WaitForSeconds(seconds);
        active = true;
    }

    protected void Update()
    {
        if (lightObjectReference != null && !isPowerInUse)
        {
            // method call we can override
            DestroyIfTooFarApartAndTriggerCooldown();
        }
    }

    // method to be overridden
    protected virtual void DestroyIfTooFarApartAndTriggerCooldown()
    {
        //print("called");
        if (Vector3.Distance(transform.position, lightObjectReference.gameObject.transform.position) > maxDistanceFromPlayer)
        {
            Destroy(lightObjectReference.gameObject);
            lightObjectReference = null;
            StartCoroutine(Cooldown(breakAwayCooldown));
        }
    }

    protected void DestroyAndNullifyLight()
    {
        Destroy(lightObjectReference.gameObject);
        lightObjectReference = null;
    }

}
