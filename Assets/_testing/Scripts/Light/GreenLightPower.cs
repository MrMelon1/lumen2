﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GreenLightPower : LightPower
{
    
    public GreenLightObject greenLightPrefab;
    [Header("Teleport Timing Settings")]
    [Tooltip("Time needed for the light to fade in, and the player to vanish.")]
    public float fadeInTime = .5f;
    [Tooltip("Time needed for the player to 'travel' to the target point.")]
    public float transitTime = 1f;
    [Tooltip("Time needed for the light to fade out after the transit time expires and the player reappears.")]
    public float fadeOutTime = .5f;
    [Header("----------")]
    [Tooltip("Script attached to the object you want to fade in an' out")]
    public FadeManager fadeController;
    public bool isTransitionLightActive = true;
    [Tooltip("If the player exceeds this distance away from the green light it gets destroyed and a 3 second cooldown is triggered.")]
    
    
    
    private GameObject player;
    private bool teleporting;

    new private void Start()
    {
        base.Start();
        player = GameObject.Find("Player");
    }


    // pressing the button spawns a green light
    // if the button is pressed again while the green light is active the player will swap positions with it
    new private void Update()
    {
        if (active)
        {
            if (Input.GetButtonDown(powerControlButton) || Input.GetKeyDown(controlBtnAltKeycode))
            {
                if (lightObjectReference == null)
                {
                    lightObjectReference = Instantiate(greenLightPrefab, transform.position, transform.rotation);
                }
                else if (lightObjectReference != null)
                {
                    StartCoroutine(teleport());
                }
            }
        }
        base.Update();

    }

    IEnumerator teleport()
    {
        GameObject targetObject = lightObjectReference.gameObject.GetComponent<GreenLightObject>().getReference();
        Vector3 playerCoord = transform.position;
        Vector3 targetCoord = targetObject.transform.position;

        isPowerInUse = true;
        // trigger cooldown
        StartCoroutine(Cooldown());
        // trigger fade in
        if(fadeController != null)
            if (isTransitionLightActive)
                fadeController.Fade(true, fadeInTime);

        yield return new WaitForSeconds(fadeInTime);
        // remove player from play temporarily
        SetIncorpreal(true);
        yield return new WaitForSeconds(transitTime);
        ////
        // This is where we need to get the reference to the object the green light is attached to //
        // get target transform coord
        targetCoord = targetObject.transform.position;
        // get player transform coord
        playerCoord = gameObject.transform.position;
        // move the player to the target
        transform.position = targetCoord;
        // the target to the player
        targetObject.transform.position = playerCoord;
        // then renable the colliders and stuff
        ////
        DestroyAndNullifyLight();
        // return player to play
        SetIncorpreal(false);
        // trigger fade out
        if (fadeController != null)
            if (isTransitionLightActive)
                fadeController.Fade(false, fadeInTime);
        yield return new WaitForSeconds(fadeOutTime);
        isPowerInUse = false;
    }

    private void SetIncorpreal(bool incorpreal)
    {
        // collider and physics component to be disabled
        Rigidbody playerPhysics = GetComponent<Rigidbody>();
        CapsuleCollider playerCollider = GetComponent<CapsuleCollider>();

        // controllers to be disabled
        MovementController playerMovement = GetComponent<MovementController>();
        JumpController playerJump = GetComponent<JumpController>();
        CameraControllerScript playerRotation = GetComponentInChildren<CameraControllerScript>();

        if (incorpreal)
        {
            // disable controllers
            playerMovement.enabled = false;
            playerJump.enabled = false;
            playerRotation.enabled = false;
            // disable physics
            playerPhysics.constraints = RigidbodyConstraints.FreezePosition;
            // disable collider
            playerCollider.enabled = false;
        }
        else
        {
            // enable collider
            playerCollider.enabled = true;
            // enable physics
            playerPhysics.constraints = RigidbodyConstraints.None;
            playerPhysics.constraints = RigidbodyConstraints.FreezeRotationX | RigidbodyConstraints.FreezeRotationZ;
            // enable controllers
            playerMovement.enabled = true;
            playerJump.enabled = true;
            playerRotation.enabled = true;
        }
    }

    override protected void DestroyIfTooFarApartAndTriggerCooldown()
    {
        GreenLightObject grnLightObj = lightObjectReference.gameObject.GetComponent<GreenLightObject>();
        if(grnLightObj)
        {
            if (Vector3.Distance(transform.position, grnLightObj.getReference().transform.position) > maxDistanceFromPlayer)
            {
                Destroy(lightObjectReference.gameObject);
                lightObjectReference = null;
                StartCoroutine(Cooldown(breakAwayCooldown));
            }
        }
    }

}