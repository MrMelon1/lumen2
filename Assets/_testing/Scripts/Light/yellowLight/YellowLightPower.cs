﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class YellowLightPower : LightPower
{
    public yellowLightObject yellowLightPrefab;        // References the object to be instantiated
	public GameObject SFX; //Sfx Manager
    public float echoDuration;
    public float echoSpeed = 10f;

    private void Start()
    {
        base.Start();
        if (SFX == null)
            print("SFX IS NULL");
    }

    // Update is called once per frame
    new void Update()
    {
        base.Update();
        if ((Input.GetButtonDown(powerControlButton) && active) || (Input.GetKeyDown(controlBtnAltKeycode) && active))
        {
            if (lightObjectReference == null)// if there is no yellow light in the level
            {
                // destroy any echos already in play
                Destroy(GameObject.Find("movingEcho"));
                Destroy(GameObject.Find("echo"));
                lightObjectReference = Instantiate(yellowLightPrefab, transform.position, transform.rotation);
                // we need to set the decay timer here for the yellow light object through the LightObject....parent..
                lightObjectReference.setDuration(echoDuration);
                if (SFX != null)
                    SFX.SendMessage("EchoDeploy");
            }
            else // if there is
            {
                lightObjectReference.Detonate(transform.rotation, echoSpeed);
                StartCoroutine(Cooldown());
                lightObjectReference = null;
                if (SFX != null)
                    SFX.SendMessage("EchoActivate");
            }
        }
    }
}
