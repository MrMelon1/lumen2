﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class echo : MonoBehaviour
{
    [Tooltip("The echo duration will reset if it teleports a distance greater than this.\nYou probably won't need to modify this value under any circumstances.")]
    public float durationResetDistance = .5f;

    private float decayTime;
    private float startTime;
    private bool isDecaying = false;
    protected Vector3 previousPosition;

    protected void Start()
    {
        previousPosition = transform.position;
    }
    // Update is called once per frame
    protected void Update()
    {
        // check to see if this object teleported
        if (hasTeleported())
        {
            resetDecay();
        }
        previousPosition = transform.position;
        if (isDecaying)
        {
            // if this is keeping track of time
            // and the time exceeds the duration
            // destroy this object
            float f = Time.time - startTime;
            if (f >= decayTime)
                Destroy(gameObject);
        }
    }

    protected bool hasTeleported()
    {
        if (Vector3.Distance(transform.position, previousPosition) > durationResetDistance)
            return true;
        else
            return false;
    }
    
    protected void selfDestruct()
    {
        Destroy(gameObject);
    }
    

    public void initiateDecay(float lifeSpan)
    {
        decayTime = lifeSpan;
        // tell the echo to start keeping track of time
        startTime = Time.time;
        isDecaying = true;
    }

    public void resetDecay()
    {
        // reset the start time
        startTime = Time.time;
    }
}
