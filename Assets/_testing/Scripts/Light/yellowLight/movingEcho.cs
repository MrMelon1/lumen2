﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movingEcho : echo
{
    public float speed = 10.0f;

    public void setSpeed(float f) { speed = f; }
    new private void Start()
    {
        base.Start();
    }
    // Update is called once per frame
    new void Update()
    {
        base.Update();
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

}
