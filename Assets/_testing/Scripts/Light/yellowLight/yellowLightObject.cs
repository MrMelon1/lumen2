﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class yellowLightObject : LightObject
{

    public GameObject echo;
    public GameObject movingEcho;

    private echo currentEcho;
    private float decayTime = 10f;
    private float echoSpeed = 10f;

    public void setSpeed(float speed) { echoSpeed = speed; }
    

    public override void Detonate(Quaternion playerRotation, float echoSpeed)
    {
        if (IsUsingAnalogSticks() || IsPressingWASD())
        {
            // get players angle of movement
            // TODO change this so that the player passes the tracker variable instead
            GameObject p = GameObject.Find("Player");
            DirectionOfMovementTracker tracker = p.GetComponent<DirectionOfMovementTracker>();

            // create echo
            currentEcho = Instantiate(movingEcho, transform.position, tracker.GetAngleOfMovement()).GetComponent<echo>();
            currentEcho.GetComponent<movingEcho>().setSpeed(echoSpeed);
        }
        else
            currentEcho = Instantiate(echo, transform.position, playerRotation).GetComponent<echo>(); // create echo
        
        currentEcho.initiateDecay(duration);
        Destroy(this.gameObject);
    }

    private bool IsPressingWASD() //For seeing if the player is moving using WASD
    {
        if (Input.GetKey("w"))
            return true;
        else if (Input.GetKey("a"))
            return true;
        else if (Input.GetKey("s"))
            return true;
        else if (Input.GetKey("d"))
            return true;
        else
            return false;
    }

    private bool IsUsingAnalogSticks()
    {
        if (Input.GetAxis("LeftJoystickX") != 0)
            return true;
        else if (Input.GetAxis("LeftJoystickY") != 0)
            return true;
        else
            return false;
    }
}
