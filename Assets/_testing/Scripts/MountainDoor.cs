﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MountainDoor : MonoBehaviour
{
    private bool wasOpened = false;
    public Button input;

    private void Update()
    {
        if (input.getIsOn() && !wasOpened)
            Destroy(gameObject);
    }
}
