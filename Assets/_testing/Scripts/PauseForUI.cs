﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PauseForUI : MonoBehaviour
{ //TODO need to find a better long term means of disabling all of Lumen's powers.
    // have public variables out for the player to drag in components
    private MovementController lumensMoveController;
    public GameObject lumensMoveControllerObj;

    private JumpController lumensJumpController;
    public GameObject lumensJumpControllerObj;

    private CameraControllerScript lumensCamController;
    public GameObject lumensCamControllerObj;

    private YellowLightPower lumensYellowLightPowerController;
    public GameObject lumensYellowLightPowerControllerObj;

    private bool isReadyToRun = false;

    private void Start()
    {
        if (lumensMoveControllerObj && lumensJumpControllerObj && lumensCamControllerObj && lumensCamControllerObj && lumensYellowLightPowerControllerObj)
        {
            lumensMoveController = lumensMoveControllerObj.GetComponent<MovementController>();
            lumensJumpController = lumensJumpControllerObj.GetComponent<JumpController>();
            lumensCamController = lumensCamControllerObj.GetComponent<CameraControllerScript>();
            lumensYellowLightPowerController = lumensYellowLightPowerControllerObj.GetComponent<YellowLightPower>();
        }
        else
            print("ERROR: ONE OR MORE OF PUBLIC VARIABLES HAVE NOT BEEN ASSIGNED YET");

        // error warnings
        if (!lumensMoveController)
            print("ERROR: GAME OBJECT DOES NOT HAVE A VALID MOVE CONTROLLER");
        else if (!lumensJumpController)
            print("ERROR: GAME OBJECT DOES NOT HAVE A VALID JUMP CONTROLLER");
        else if (!lumensCamController)
            print("ERROR: GAME OBJECT DOES NOT HAVE A VALID CAMERA CONTROLLER");
        else if (!lumensYellowLightPowerController)
            print("ERROR: GAME OBJECT DOES NOT HAVE A VALID YELLOW LIGHT POWER CONTROLLER");
        else
            isReadyToRun = true;

        // starting game off with everything configured right for gameplay (should get rid of the cursor problem)
        Cursor.visible = false;
        Cursor.lockState = CursorLockMode.Locked;
        lumensMoveController.enabled = true;
        lumensJumpController.enabled = true;
        lumensCamController.enabled = true;
        lumensYellowLightPowerController.enabled = true;
    }

    private void Update()
    {
        if (isReadyToRun)
        {
            if (Input.GetKeyDown(KeyCode.BackQuote))
            {
                if (Cursor.visible && Cursor.lockState == CursorLockMode.None)
                {
                    // everything set up for gameplay
                    setControlsEnabled(true);
                }
                else
                {
                    // everything set up for menu usage
                    setControlsEnabled(false);
                }
            }
        }
    }

    public void setControlsEnabled(bool isEnabled)
    {
        if(isEnabled == true)
        {
            // enable all controls and stuff
            Cursor.visible = false;
            Cursor.lockState = CursorLockMode.Locked;
            lumensMoveController.enabled = true;
            lumensJumpController.enabled = true;
            lumensCamController.enabled = true;
            lumensYellowLightPowerController.enabled = true;
        }
        else if(isEnabled == false)
        {
            // disable all controls and stuff
            Cursor.visible = true;
            Cursor.lockState = CursorLockMode.None;
            lumensMoveController.enabled = false;
            lumensJumpController.enabled = false;
            lumensCamController.enabled = false;
            lumensYellowLightPowerController.enabled = false;
        }
    }
}
