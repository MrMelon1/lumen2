﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RemoteKillzone : MonoBehaviour
{
    public float killHeight;
    public HealthBar playerHealthBar;

    private int overKill = 999;

    private void Start()
    {
        playerHealthBar = GameObject.Find("Player").GetComponent<HealthBar>();
    }

    private void Update()
    {
        if (playerHealthBar.gameObject.transform.position.y < killHeight)
            playerHealthBar.dealDamage(overKill);
    }
}
