﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SafeZone : MonoBehaviour
{
    [SerializeField]
    private int uses = 2;

    public int getUses() { return uses; }
    public void setUses(int number) { uses = number; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player" && other.gameObject.GetComponent<CheckpointSpawnee>())
            other.gameObject.GetComponent<CheckpointSpawnee>().setLastTouchedSafeZone(this);
    }
}
