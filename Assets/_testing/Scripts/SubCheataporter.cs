﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SubCheataporter : MonoBehaviour
{
    private bool isTouched = false;
    private bool hasPermission = true;

    public bool getIsTouched() { return isTouched; }
    public void setIsTouched(bool b) { isTouched = b; }

    public bool getHasPermission() { return hasPermission; }
    public void setHasPermission(bool b) { hasPermission = b; }

    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.name == "Player" && hasPermission)
            isTouched = true;
    }

    private void OnTriggerExit(Collider other)
    {
        if (other.gameObject.name == "Player")
            hasPermission = true;
    }
}
