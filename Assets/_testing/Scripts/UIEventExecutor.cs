﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class UIEventExecutor : MonoBehaviour
{
    public Shader toonShader;
    public Shader standardShader;
    public Material playerModelMaterial;
    public Material blankMaterial;
    public List<Renderer> renderersToChange;

    private int firstWordIndex = 0;
    private int secondWordIndex = 1;
    private int thirdWordIndex = 2;
    private int fourthWordIndex = 3;
    private int fifthWordIndex = 4;
    private int sixthWordIndex = 5;

    private string output = "invalid input";

    struct shaderSettingTracker
    {
        public Color diffuseColor;
        public Color unlitColor;
        public Color specularColor;
        public float threshold;
        public float shine;
        public float outline;
    }
    private shaderSettingTracker shaderTracker;

    private void Start()
    {
        resetShaderTracker();
    }

    // this is for keeping track of output settings
    private void resetShaderTracker()
    {
        shaderTracker.diffuseColor = Color.white;
        shaderTracker.unlitColor = new Color(.5f, .5f, .5f, 1);
        shaderTracker.specularColor = Color.white;
        shaderTracker.threshold = .1f;
        shaderTracker.shine = 1f;
        shaderTracker.outline = .1f;
    }


    public string performActionAndReturnOutput(List<string> line)
    {
        
        switch (line[0])
        {
            case "greet":
                output = "Hello world!";
                break;
            case "speed":
                GameObject.Find("Player").GetComponent<MovementController>().speed = float.Parse(line[1]);
                output = "Speed changed to " + float.Parse(line[1]);
                break;
            case "jump":
                GameObject.Find("Player").GetComponent<JumpController>().jumpForce = float.Parse(line[1]);
                output = "Jump force changed to " + float.Parse(line[1]);
                break;
            case "shine":
                float newShininess = float.Parse(line[secondWordIndex]);
                float maxShine = 1f;
                float minShine = .5f;
                if (newShininess > maxShine)
                    newShininess = maxShine;
                else if (newShininess < minShine)
                    newShininess = minShine;
                playerModelMaterial.SetFloat("_Shininess", newShininess);
                blankMaterial.SetFloat("_Shininess", newShininess);
                output = "The shininess has been changed!";
                break;
            case "toon":
                playerModelMaterial.shader = toonShader;
                blankMaterial.shader = toonShader;
                output = "Shader set to toon mode.";
                break;
            case "basic":
                playerModelMaterial.shader = standardShader;
                blankMaterial.shader = standardShader;
                output = "Shader set to standard mode.";
                break;
            case "blank":
                for(int i = 0; i < renderersToChange.Count; i++)
                {
                    renderersToChange[i].material = blankMaterial;
                }
                output = "Material(s) set to blank.";
                break;
            case "texture":
                for (int i = 0; i < renderersToChange.Count; i++)
                {
                    renderersToChange[i].material = playerModelMaterial;
                }
                output = "Material(s) set to map.";
                break;
            case "outline":
                float newThickness = float.Parse(line[secondWordIndex]);
                float maxThickness = 1;
                float minThickness = 0;
                if (newThickness > maxThickness)
                    newThickness = maxThickness;
                else if (newThickness < minThickness)
                    newThickness = minThickness;
                playerModelMaterial.SetFloat("_OutlineThickness", newThickness);
                blankMaterial.SetFloat("_OutlineThickness", newThickness);
                output = "The outline has been increased!";
                break;
            case "color":
                ChangeColorAndUpdateOutput(line);
                // output will be set in the change color method
                break;
            case "print":
                output = "Diffuse Color: " + shaderTracker.diffuseColor.ToString();
                output += "\nUnlit Color: " + shaderTracker.unlitColor.ToString();
                output += "\nSpecular Color: " + shaderTracker.specularColor.ToString();
                output += "\nThreshold: " + shaderTracker.threshold.ToString();
                output += "\nShininess: " + shaderTracker.shine.ToString();
                output += "\nOutline: " + shaderTracker.outline.ToString();
                break;
            case "threshold":
                float newThreshold = float.Parse(line[secondWordIndex]);
                float thresholdMin = -1.1f;
                float thresholdMax = 1f;
                if (newThreshold > thresholdMax)
                    newThreshold = 1f;
                else if (newThreshold < thresholdMin)
                    newThreshold = -1.1f;
                shaderTracker.threshold = newThreshold;
                playerModelMaterial.SetFloat("_DiffuseThreshold", newThreshold);
                blankMaterial.SetFloat("_DiffuseThreshold", newThreshold);
                output = "The threshold has been changed!";
                break;
            case "reset":
                ResetAllToDefault();
                break;

        }
        return output;
    }

    private void ChangeColorAndUpdateOutput(List<string> line)
    {

        float r = 1f;
        float g = 1f;
        float b = 1f;
        float a = 1f;
        Color inputColor = Color.white;

        // if the player remembers to fill in an RGBA value
        if (!(line.Count < 3))
        {
            r = float.Parse(line[thirdWordIndex]);
            if (r > 255)
                r = 255;
            else if (r < 0)
                r = 0;
            r = r / 255;

            g = float.Parse(line[fourthWordIndex]);
            if (g > 255)
                g = 255;
            else if (g < 0)
                g = 0;
            g = g / 255;

            b = float.Parse(line[fifthWordIndex]);
            if (b > 255)
                b = 255;
            else if (b < 0)
                b = 0;
            b = b / 255;


            a = float.Parse(line[sixthWordIndex]);
            if (a > 1)
                a = 1;
            else if (a < 0)
                a = 0;

            inputColor = new Color(r, g, b, a);
        }
        output = "Placeholder text!";
        // if the second word is diffuse
        // change the diffuse color based off words 3-5
        // update the output

        // if the second word is unlit
        // change the unlit color based off words 3-5
        // update the output

        // if the second word is specular
        // change the shine color based off words 3-5
        // update the output

        switch (line[secondWordIndex])
        {
            case "diffuse":
                playerModelMaterial.SetColor("_Color", inputColor);
                blankMaterial.SetColor("_Color", inputColor);
                output = "diffuse changed to ";
                addRGBAToOutput(r,g,b,a);
                shaderTracker.diffuseColor = inputColor;
                break;
            case "unlit":
                playerModelMaterial.SetColor("_UnlitColor", inputColor);
                blankMaterial.SetColor("_UnlitColor", inputColor);
                output = "unlit changed to ";
                addRGBAToOutput(r, g, b, a);
                shaderTracker.unlitColor = inputColor;
                break;
            case "specular":
                playerModelMaterial.SetColor("_SpecColor", inputColor);
                blankMaterial.SetColor("_SpecColor", inputColor);
                output = "specular changed to ";
                addRGBAToOutput(r, g, b, a);
                shaderTracker.specularColor = inputColor;
                break;
        }
    }

    private void addRGBAToOutput(float r, float g, float b, float a)
    {
        output += "\nred: ";
        output += r.ToString();
        output += "\ngreen: ";
        output += g.ToString();
        output += "\nblue: ";
        output += b.ToString();
        output += "\nalpha: ";
        output += a.ToString();
    }

    private void ResetAllToDefault()
    {
        // reset colors
        Color grey = new Color(.5f, .5f, .5f, .5f);
        playerModelMaterial.SetColor("_Color", Color.white);
        blankMaterial.SetColor("_Color", Color.white);
        playerModelMaterial.SetColor("_UnlitColor", grey);
        blankMaterial.SetColor("_UnlitColor", grey);
        playerModelMaterial.SetColor("_SpecColor", Color.white);
        blankMaterial.SetColor("_SpecColor", Color.white);
        output = "Colors reset to default values.";

        // reset threshold
        float defaultThreshold = .1f;
        playerModelMaterial.SetFloat("_DiffuseThreshold", defaultThreshold);
        blankMaterial.SetFloat("_DiffuseThreshold", defaultThreshold);

        // reset material
        for (int i = 0; i < renderersToChange.Count; i++)
        {
            renderersToChange[i].material = playerModelMaterial;
        }

        // reset tracker
        resetShaderTracker();

        // reset shine
        float defaultShine = 1;
        playerModelMaterial.SetFloat("_Shininess", defaultShine);
        blankMaterial.SetFloat("_Shininess", defaultShine);

        //reset outline
        float defaultOutlineThickness = .1f;
        playerModelMaterial.SetFloat("_OutlineThickness", defaultOutlineThickness);
        blankMaterial.SetFloat("_OutlineThickness", defaultOutlineThickness);


        output = "All settings have been reverted to their default values.";
    }

}
