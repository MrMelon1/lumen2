﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class movement : MonoBehaviour
{

    public float speed = 10.0f;
    public Rigidbody rb;
    private float straffe;
    private float translation;

    // Start is called before the first frame update
    void Start()
    {
    }

    // Update is called once per frame
    void Update()
    {
        transform.Translate(Vector3.forward * Time.deltaTime * speed);
    }

    private void getPlayerStraffe()
    {
        straffe = Input.GetAxis("Horizontal") * speed;
    }

    private void getPlayerTranslation()
    {
        translation = Input.GetAxis("Vertical") * speed;
    }


}
